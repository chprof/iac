var carouselSpecialistsSelector = '[data-carousel="specialists"]',
    carouselAboutSelector = '[data-carousel="about"]',
    carouselOfferSelector = '[data-carousel="offer"]',
    carouselSpecialistsConfig = {
      direction: 'horizontal',
      slidesPerView: 4,
      loop: true,
      speed: 400,
      spaceBetween: 70,
      navigation: {
        nextEl: '.swiper-button-next--specialists',
        prevEl: '.swiper-button-prev--specialists',
      },
      breakpoints: {
        0: {
          slidesPerView: 1,
          spaceBetween: 15,
        },
        768: {
          slidesPerView: 2,
          spaceBetween: 15,
        },
        992: {
          spaceBetween: 15,
          slidesPerView: 3,
        },
        1200: {
          slidesPerView: 4,
          spaceBetween: 15,
        },
        1655: {
          spaceBetween: 70,
        },
      }
    },
    carouselAboutConfig = {
      direction: 'horizontal',
      slidesPerView: 1,
      centeredSlides: true,
      loop: true,
      spaceBetween: 0,
      speed: 400,
      navigation: {
        nextEl: '.swiper-button-next--about',
        prevEl: '.swiper-button-prev--about',
      },
      breakpoints: {
        0: {
          slidesPerView: 1,
          spaceBetween: 15,
          centeredSlides: false,
          autoHeight: true
        },
        992: {
          slidesPerView: 3,
          centeredSlides: true,
        },
      }
    }
    carouselOfferConfig = {
      direction: 'horizontal',
      slidesPerView: 1,
      loop: true,
      spaceBetween: 0,
      speed: 400,
      effect: 'fade',
      autoplay: {
        delay: 3000,
      },
      pagination: {
        el: '.swiper-pagination--offer',
      },
    }

function toggleElem(elem) {
  if(elem) {
    elem.addEventListener('click', function(e) {
      e.preventDefault();
      const _this = e.currentTarget;
      const _target = _this.dataset.target;
      const _targetElement = document.querySelector(_target);
      const _targetElementInput = _targetElement.querySelector('.form-search-header__input');
      const _targetElementForm = _targetElement.querySelector('.form-search-header__form');
      if(_targetElement.classList.contains('form-search-header')) {
        const clientWidth = window.innerWidth;
        const formSeachRect = _targetElement.getBoundingClientRect();
        console.log(formSeachRect);
        if(!_targetElementForm.style.right && !_targetElementForm.style.left) {
          // _targetElementForm.style.right = clientWidth - formSeachRect.right - formSeachRect.width + 'px';
          _targetElementForm.style.left = -formSeachRect.left + 'px';
        } else {
          _targetElementForm.style.right = null;
          _targetElementForm.style.left = null;
        }
      }
      if(_targetElement.classList.contains('active') && _targetElement.classList.contains('form-search-header') && _targetElementInput && _targetElementInput.value) {
        _this.type = 'submit';
        _targetElementForm.submit()
      } else {

        _targetElement.classList.toggle('active');
      }
    })
  }
};

document.addEventListener("DOMContentLoaded", () => {
  // console.log(Swiper);
  var selects = [],
      carouselSpecialists,
      formSearchToggle = document.querySelector('.form-search-header__button[data-toggle]'),
      menuToggle = document.querySelector('.button-menu[data-toggle]'),
      closeToggle = document.querySelector('.list-menu__close[data-toggle]'),
      selectElem = document.querySelector('[data-select]'),
      selectElems = document.querySelectorAll('[data-select]');
  svg4everybody();
  if(selectElem) {
    [].slice.call(selectElems).forEach(function(element) {
      const elementClass = element.dataset.selectClass
      selects.push(new Choices(element, {
        placeholder: true,
        classNames: {
          containerOuter: `choices ${[elementClass]}`,
        },
        loadingText: 'Загрузка...',
        noResultsText: 'Результаты не найдены',
        noChoicesText: 'Нет вариантов выбора',
        itemSelectText: 'Нажмите, чтобы выбрать'
        // loadingText: 'Loading...',
        // noResultsText: 'No results found',
        // noChoicesText: 'No choices to choose from',
        // itemSelectText: 'Press to select',
      }))
    })
  };
  toggleElem(formSearchToggle);
  toggleElem(menuToggle);
  toggleElem(closeToggle);
  carouselSpecialists = new Swiper(carouselSpecialistsSelector, carouselSpecialistsConfig);
  carouselAbout = new Swiper(carouselAboutSelector, carouselAboutConfig);
  carouselOffer = new Swiper(carouselOfferSelector, carouselOfferConfig);
  console.log(carouselSpecialists);
});